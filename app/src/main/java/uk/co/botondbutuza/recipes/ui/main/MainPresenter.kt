package uk.co.botondbutuza.recipes.ui.main

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import uk.co.botondbutuza.recipes.common.data.models.Recipe
import uk.co.botondbutuza.recipes.common.data.repository.RecipeBook
import uk.co.botondbutuza.recipes.ui.main.MainContract.Presenter.Complexity
import uk.co.botondbutuza.recipes.ui.main.MainContract.Presenter.Timeframe
import javax.inject.Inject

class MainPresenter @Inject constructor(
    private val repository: RecipeBook,
    private val view: MainContract.View
) : MainContract.Presenter {
    private val subscriptions = CompositeDisposable()


    override fun unsubscribe() {
        subscriptions.clear()
    }


    override fun requestRecipes(query: String, complexity: Complexity, time: Timeframe) {
        Timber.e("q=$query, c=$complexity, t=$time")

        subscriptions.add(
            repository
                .getRecipes()
                // flatten out
                .flatMapObservable { Observable.fromIterable(it) }
                // filter by query
                .filter { it.query(query) }
                // filter by complexity
                .filter { it.isComplexEnough(complexity) }
                // filter by time
                .filter { it.isBetweenTimeframe(time) }
                // list it up again
                .toList()
                .subscribe(
                    view::onRecipesReady,
                    view::onError
                )
        )
    }


    // Extension functions on Recipe.

    private fun Recipe.query(q: String): Boolean {
        return this.name.contains(q) ||
            this.ingredients.any { it.name.contains(q) } ||
            this.steps.any { it.contains(q) }
    }

    /**
     * We assume complexity is based on number of ingredients as defined below.
     */
    private fun Recipe.isComplexEnough(complexity: Complexity): Boolean = when (complexity) {
        Complexity.ALL -> true
        Complexity.EASY -> this.ingredients.size < 4
        Complexity.MEDIUM -> this.ingredients.size in 4..6
        Complexity.HARD -> this.ingredients.size > 6
    }

    private fun Recipe.isBetweenTimeframe(time: Timeframe): Boolean = when (time) {
        Timeframe.ALL -> true
        Timeframe.SHORT -> this.timers.sum() < 10
        Timeframe.MEDIUM -> this.timers.sum() in 10..20
        Timeframe.LONG -> this.timers.sum() > 20
    }
}