package uk.co.botondbutuza.recipes.common.data

import io.reactivex.Single
import retrofit2.http.GET
import uk.co.botondbutuza.recipes.common.data.models.Recipe

interface RecipesApi {

    @GET("recipes.json")
    fun recipes(): Single<List<Recipe>>
}
