package uk.co.botondbutuza.recipes.ui.recipe

import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_recipe.*
import kotlinx.android.synthetic.main.item_recipe_details.*
import timber.log.Timber
import uk.co.botondbutuza.recipes.R
import uk.co.botondbutuza.recipes.common.data.models.Recipe
import uk.co.botondbutuza.recipes.common.load
import uk.co.botondbutuza.recipes.common.ui.BaseActivity
import javax.inject.Inject

class RecipeActivity : BaseActivity(R.layout.activity_recipe), RecipeContract.View {

    companion object {

        private const val EXTRA_RECIPE_NAME = "EXTRA_RECIPE_NAME"

        internal fun launch(context: Context, recipe: Recipe) {
            val intent = Intent(context, RecipeActivity::class.java)
            intent.putExtra(EXTRA_RECIPE_NAME, recipe.name)
            context.startActivity(intent)
        }
    }


    @Inject internal lateinit var presenter: RecipePresenter
    @Inject internal lateinit var ingredientsAdapter: IngredientAdapter
    @Inject internal lateinit var instructionsAdapter: InstructionAdapter
    private val recipeId by lazy { intent.getStringExtra(EXTRA_RECIPE_NAME) }


    override fun initViews() {
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        ingredients.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        ingredients.adapter = ingredientsAdapter

        instructions.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        instructions.adapter = instructionsAdapter

        presenter.requestRecipe(recipeId)
    }

    override fun teardown() {
        presenter.unsubscribe()
    }

    override fun injectDagger() {
        DaggerRecipeComponent.builder()
            .repositoryComponent(app().repositoryComponent)
            .recipeModule(RecipeModule(this))
            .build().inject(this)
    }


    // RecipeContract.View implementation.

    override fun onRecipeReady(recipe: Recipe) {
        Timber.e("time=${recipe.createdAt}")
        toolbarImage.load(recipe.imageURL)
        name.text = recipe.name
        ingredientsLabel.text = getString(R.string.recipe_ingredient_num, recipe.ingredients.size)

        ingredientsAdapter.setItems(recipe.ingredients)
        instructionsAdapter.setItems(recipe.steps)
    }
}