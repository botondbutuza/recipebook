package uk.co.botondbutuza.recipes.common.dagger.component


import android.app.Activity
import android.content.Context
import dagger.Component
import uk.co.botondbutuza.recipes.common.dagger.module.AppModule
import uk.co.botondbutuza.recipes.common.dagger.module.LocalDataSourceModule
import uk.co.botondbutuza.recipes.common.dagger.module.RemoteDataSourceModule
import uk.co.botondbutuza.recipes.common.dagger.module.RepositoryModule
import uk.co.botondbutuza.recipes.common.data.repository.RecipeBook
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, RepositoryModule::class, LocalDataSourceModule::class, RemoteDataSourceModule::class])
interface RepositoryComponent {

    val repository: RecipeBook

    val context: Context

    fun inject(activity: Activity)
}
