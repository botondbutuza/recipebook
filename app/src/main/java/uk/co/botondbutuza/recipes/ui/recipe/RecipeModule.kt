package uk.co.botondbutuza.recipes.ui.recipe

import dagger.Module
import dagger.Provides

@Module
class RecipeModule(private val view: RecipeContract.View) {

    @Provides fun provideView() = view
}