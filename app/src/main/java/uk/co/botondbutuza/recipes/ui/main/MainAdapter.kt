package uk.co.botondbutuza.recipes.ui.main

import android.view.View
import kotlinx.android.synthetic.main.item_recipe.view.*
import uk.co.botondbutuza.recipes.R
import uk.co.botondbutuza.recipes.common.data.models.Recipe
import uk.co.botondbutuza.recipes.common.load
import uk.co.botondbutuza.recipes.common.ui.BaseRecyclerViewAdapter
import uk.co.botondbutuza.recipes.common.ui.BaseRecyclerViewHolder
import javax.inject.Inject

class MainAdapter @Inject constructor() : BaseRecyclerViewAdapter<Recipe, MainAdapter.Holder>(R.layout.item_recipe) {

    override fun createViewHolderForView(view: View, viewType: Int) = Holder(view)


    class Holder(itemView: View) : BaseRecyclerViewHolder<Recipe>(itemView) {

        override fun bind(item: Recipe) {
            itemView.title.text = item.name
            itemView.ingredientsLabel.text = itemView.context.getString(R.string.recipe_num_ingredient, item.ingredients.size)
            itemView.minutes.text = itemView.context.getString(R.string.recipe_time_minutes, item.timers.sum())
            itemView.image.load(item.imageURL)
        }

        override fun unbind() {
            itemView.image.setImageDrawable(null)
        }
    }
}