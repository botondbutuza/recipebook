package uk.co.botondbutuza.recipes.ui.recipe

import android.view.View
import kotlinx.android.synthetic.main.item_textview.view.*
import uk.co.botondbutuza.recipes.R
import uk.co.botondbutuza.recipes.common.ui.BaseRecyclerViewAdapter
import uk.co.botondbutuza.recipes.common.ui.BaseRecyclerViewHolder
import javax.inject.Inject

class InstructionAdapter @Inject constructor() : BaseRecyclerViewAdapter<String, InstructionAdapter.Holder>(R.layout.item_textview) {

    override fun createViewHolderForView(view: View, viewType: Int) = Holder(view)


    class Holder(itemView: View) : BaseRecyclerViewHolder<String>(itemView) {

        override fun bind(item: String) {
            itemView.text.text = item
        }

        override fun unbind() {
        }
    }
}