package uk.co.botondbutuza.recipes.common

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.EditText
import android.widget.ImageView
import android.widget.Spinner
import com.squareup.picasso.Picasso
import uk.co.botondbutuza.recipes.common.data.models.Recipe


fun ImageView.load(url: String) {
    Picasso
        .get()
        .load(url)
        .into(this)
}

fun Recipe.isTooOld(): Boolean {
    return System.currentTimeMillis() - createdAt > Constants.CACHE_TIME_MS
}

inline fun Spinner.onSelection(crossinline itemSelected: (item: String) -> Unit) {
    onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onNothingSelected(parent: AdapterView<*>?) {}

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            itemSelected(adapter.getItem(position) as String)
        }
    }
}

inline fun EditText.onTextChanged(crossinline afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            afterTextChanged(text.toString())
        }

        override fun afterTextChanged(editable: Editable?) {}
    })
}