package uk.co.botondbutuza.recipes.ui.main

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import uk.co.botondbutuza.recipes.R
import uk.co.botondbutuza.recipes.common.data.models.Recipe
import uk.co.botondbutuza.recipes.common.onSelection
import uk.co.botondbutuza.recipes.common.onTextChanged
import uk.co.botondbutuza.recipes.common.ui.BaseActivity
import uk.co.botondbutuza.recipes.common.ui.OnRecyclerItemTouchListener
import uk.co.botondbutuza.recipes.ui.recipe.RecipeActivity
import javax.inject.Inject

class MainActivity : BaseActivity(R.layout.activity_main), MainContract.View, OnRecyclerItemTouchListener.OnItemClickListener {

    companion object {
        private const val COLUMN_NUMBER = 2
    }


    @Inject internal lateinit var presenter: MainPresenter
    @Inject internal lateinit var adapter: MainAdapter
    private val itemTouchListener by lazy { OnRecyclerItemTouchListener(this, this) }


    override fun initViews() {
        scroll.layoutManager = GridLayoutManager(this, COLUMN_NUMBER)
        scroll.adapter = adapter
        scroll.addOnItemTouchListener(itemTouchListener)
        refresh.setOnRefreshListener { requestRecipes() }

        // spinners fire off the selection listener on layout, which is bad, so let's get that out of the way here
        complexityFilter.setSelection(0, false)
        timeFilter.setSelection(0, false)

        // set up listeners
        query.onTextChanged { requestRecipes() }
        complexityFilter.onSelection { requestRecipes() }
        timeFilter.onSelection { requestRecipes() }

        presenter.requestRecipes()
    }

    override fun teardown() {
        presenter.unsubscribe()
        scroll.removeOnItemTouchListener(itemTouchListener)

        complexityFilter.onItemSelectedListener = null
        timeFilter.onItemSelectedListener = null
        refresh.setOnRefreshListener(null)
    }

    override fun injectDagger() {
        DaggerMainComponent.builder()
            .repositoryComponent(app().repositoryComponent)
            .mainModule(MainModule(this))
            .build().inject(this)
    }


    // OnRecyclerItemTouchListener.OnItemClickListener implementation.

    override fun onItemClick(view: View, viewHolder: RecyclerView.ViewHolder, position: Int): Boolean {
        RecipeActivity.launch(this, adapter.getItemAt(position))
        return true
    }


    // MainContract.View implementation.

    override fun onRecipesReady(recipes: List<Recipe>) {
        refresh.isRefreshing = false
        adapter.setItems(recipes)
    }


    // Internal.

    private fun requestRecipes() {
        val query = query.text.toString()
        val complexity = convertComplexity(complexityFilter.selectedItem.toString())
        val timeframe = convertTimeframe(timeFilter.selectedItem.toString())

        presenter.requestRecipes(query, complexity, timeframe)
    }

    private fun convertComplexity(what: String): MainContract.Presenter.Complexity = when (what) {
        getString(R.string.complexity_easy) -> MainContract.Presenter.Complexity.EASY
        getString(R.string.complexity_medium) -> MainContract.Presenter.Complexity.MEDIUM
        getString(R.string.complexity_hard) -> MainContract.Presenter.Complexity.HARD
        else -> MainContract.Presenter.Complexity.ALL
    }

    private fun convertTimeframe(what: String): MainContract.Presenter.Timeframe = when (what) {
        getString(R.string.time_short) -> MainContract.Presenter.Timeframe.SHORT
        getString(R.string.time_medium) -> MainContract.Presenter.Timeframe.MEDIUM
        getString(R.string.time_long) -> MainContract.Presenter.Timeframe.LONG
        else -> MainContract.Presenter.Timeframe.ALL
    }
}
