package uk.co.botondbutuza.recipes.ui.recipe

import io.reactivex.disposables.CompositeDisposable
import uk.co.botondbutuza.recipes.common.data.repository.RecipeBook
import javax.inject.Inject

class RecipePresenter @Inject constructor(
    private val repository: RecipeBook,
    private val view: RecipeContract.View
) : RecipeContract.Presenter {
    private val subscriptions = CompositeDisposable()

    override fun unsubscribe() {
        subscriptions.clear()
    }


    override fun requestRecipe(id: String) {
        subscriptions.add(
            repository
                .getRecipe(id)
                .subscribe(
                    view::onRecipeReady,
                    view::onError
                )
        )
    }
}