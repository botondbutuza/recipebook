package uk.co.botondbutuza.recipes.ui.main

import uk.co.botondbutuza.recipes.common.data.models.Recipe
import uk.co.botondbutuza.recipes.common.ui.BaseContract

interface MainContract {

    interface View : BaseContract.View {

        fun onRecipesReady(recipes: List<Recipe>)
    }

    interface Presenter : BaseContract.Presenter {

        enum class Complexity { ALL, EASY, HARD, MEDIUM }
        enum class Timeframe { ALL, SHORT, LONG, MEDIUM }


        fun requestRecipes(query: String = "", complexity: Complexity = Complexity.ALL, time: Timeframe = Timeframe.ALL)
    }
}