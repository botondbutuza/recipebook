package uk.co.botondbutuza.recipes.common.data

import io.reactivex.Single
import uk.co.botondbutuza.recipes.common.data.models.Recipe


/**
 * Created by brotond on 26/01/2018.
 */

interface RemoteDataSource {

    fun recipes(): Single<List<Recipe>>
}
