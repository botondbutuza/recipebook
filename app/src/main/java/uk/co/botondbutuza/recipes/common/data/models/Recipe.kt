package uk.co.botondbutuza.recipes.common.data.models

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Recipe(
    @PrimaryKey var name: String = "",
    var ingredients: RealmList<Ingredient> = RealmList(),
    var steps: RealmList<String> = RealmList(),
    var timers: RealmList<Int> = RealmList(),       // minutes
    var imageURL: String = "",
    var originalURL: String = "",
    var createdAt: Long = 0
) : RealmObject()