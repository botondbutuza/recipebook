package uk.co.botondbutuza.recipes.ui.recipe

import uk.co.botondbutuza.recipes.common.data.models.Recipe
import uk.co.botondbutuza.recipes.common.ui.BaseContract

interface RecipeContract {

    interface View : BaseContract.View {

        fun onRecipeReady(recipe: Recipe)
    }

    interface Presenter : BaseContract.Presenter {

        fun requestRecipe(id: String)
    }
}