package uk.co.botondbutuza.recipes.common

class Constants {

    companion object {
        const val CACHE_TIME_MS = 1000 * 60 * 60        // one hour in ms
    }
}