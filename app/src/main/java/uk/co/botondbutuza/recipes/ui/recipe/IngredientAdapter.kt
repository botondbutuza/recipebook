package uk.co.botondbutuza.recipes.ui.recipe

import android.view.View
import kotlinx.android.synthetic.main.item_ingredient.view.*
import uk.co.botondbutuza.recipes.R
import uk.co.botondbutuza.recipes.common.data.models.Ingredient
import uk.co.botondbutuza.recipes.common.ui.BaseRecyclerViewAdapter
import uk.co.botondbutuza.recipes.common.ui.BaseRecyclerViewHolder
import javax.inject.Inject

class IngredientAdapter @Inject constructor() : BaseRecyclerViewAdapter<Ingredient, IngredientAdapter.Holder>(R.layout.item_ingredient) {

    override fun createViewHolderForView(view: View, viewType: Int) = Holder(view)


    inner class Holder(itemView: View) : BaseRecyclerViewHolder<Ingredient>(itemView) {

        override fun bind(item: Ingredient) {
            itemView.item.text = item.name
            itemView.quantity.text = item.quantity
        }

        override fun unbind() {
        }
    }
}