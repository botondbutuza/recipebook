package uk.co.botondbutuza.recipes.ui.recipe

import dagger.Component
import uk.co.botondbutuza.recipes.common.dagger.component.RepositoryComponent
import uk.co.botondbutuza.recipes.common.dagger.scope.ActivityScope

@ActivityScope
@Component(dependencies = [RepositoryComponent::class], modules = [RecipeModule::class])
interface RecipeComponent {

    fun inject(activity: RecipeActivity)
}