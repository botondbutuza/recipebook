package uk.co.botondbutuza.recipes.common.ui

interface BaseContract {

    interface View {

        fun onError(e: Throwable)
    }

    interface Presenter {

        fun unsubscribe()
    }
}