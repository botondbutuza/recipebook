package uk.co.botondbutuza.recipes.common.data.repository

import io.reactivex.Single
import uk.co.botondbutuza.recipes.common.dagger.scope.Local
import uk.co.botondbutuza.recipes.common.dagger.scope.Remote
import uk.co.botondbutuza.recipes.common.data.LocalDataSource
import uk.co.botondbutuza.recipes.common.data.RemoteDataSource
import uk.co.botondbutuza.recipes.common.data.models.Recipe


class RecipeBook(
    @param:Local @field:Local private val localDataSource: LocalDataSource,
    @param:Remote @field:Remote private val remoteDataSource: RemoteDataSource
) {

    fun getRecipes(): Single<List<Recipe>> {
        return Single
            // go local first, if nothing there, go off to remote
            .amb(listOf(
                localDataSource
                    .recipes()
                    .toSingle(),
                remoteDataSource
                    .recipes()
                    // once we've grabbed 'em, plop 'em into our basket
                    .doOnSuccess(localDataSource::upsert)
            ))
    }

    fun getRecipe(id: String): Single<Recipe> {
        return localDataSource
            .recipe(id)
    }
}
