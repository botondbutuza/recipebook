package uk.co.botondbutuza.recipes.common.data.repository

import io.reactivex.Maybe
import io.reactivex.Single
import io.realm.Realm
import io.realm.RealmObject
import io.realm.Sort
import uk.co.botondbutuza.recipes.common.data.LocalDataSource
import uk.co.botondbutuza.recipes.common.data.models.Recipe
import uk.co.botondbutuza.recipes.common.isTooOld


class LocalDataSourceImpl(private val realm: Realm) : LocalDataSource {

    override fun upsert(item: RealmObject) {
        realm.beginTransaction()
        realm.insertOrUpdate(item)
        realm.commitTransaction()
    }

    override fun upsert(items: List<RealmObject>) {
        realm.beginTransaction()
        realm.insertOrUpdate(items)
        realm.commitTransaction()
    }

    override fun clear(clazz: Class<out RealmObject>) {
        realm.beginTransaction()
        realm.delete(clazz)
        realm.commitTransaction()
    }

    override fun recipes(): Maybe<List<Recipe>> {
        val recipes = realm.where(Recipe::class.java).sort("createdAt", Sort.ASCENDING).findAll()

        return when {
            recipes.isEmpty() -> Maybe.never()
            recipes.first()!!.isTooOld() -> Maybe.never()
            else -> Maybe.just(recipes)
        }
    }

    override fun recipe(name: String): Single<Recipe> {
        val recipe = realm.where(Recipe::class.java).equalTo("name", name).findFirst()

        return when (recipe == null) {
            true -> Single.error(Exception("Recipe $name not found in local repo."))
            else -> Single.just(recipe)
        }
    }
}
