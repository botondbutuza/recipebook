package uk.co.botondbutuza.recipes.common.data.repository

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import uk.co.botondbutuza.recipes.common.data.RecipesApi
import uk.co.botondbutuza.recipes.common.data.RemoteDataSource
import uk.co.botondbutuza.recipes.common.data.models.Recipe

class RemoteDataSourceImpl(private val api: RecipesApi) : RemoteDataSource {

    override fun recipes(): Single<List<Recipe>> {
        return api
            .recipes()
            // threading
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .flatMapObservable { Observable.fromIterable(it) }
            // timestamp each item
            .map { recipe ->
                recipe.createdAt = System.currentTimeMillis()
                return@map recipe
            }
            .toList()
    }
}