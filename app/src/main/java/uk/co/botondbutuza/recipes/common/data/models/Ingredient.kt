package uk.co.botondbutuza.recipes.common.data.models

import io.realm.RealmObject

open class Ingredient(
    var quantity: String = "",
    var name: String = "",
    var type: String = ""
) : RealmObject()