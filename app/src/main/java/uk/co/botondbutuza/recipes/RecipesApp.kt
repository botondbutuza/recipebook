package uk.co.botondbutuza.recipes

import android.app.Application
import timber.log.Timber
import uk.co.botondbutuza.recipes.common.dagger.component.DaggerRepositoryComponent
import uk.co.botondbutuza.recipes.common.dagger.component.RepositoryComponent
import uk.co.botondbutuza.recipes.common.dagger.module.AppModule
import uk.co.botondbutuza.recipes.common.dagger.module.LocalDataSourceModule
import uk.co.botondbutuza.recipes.common.dagger.module.RemoteDataSourceModule

class RecipesApp : Application() {

    internal lateinit var repositoryComponent: RepositoryComponent

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        repositoryComponent = DaggerRepositoryComponent.builder()
            .appModule(AppModule(this))
            .localDataSourceModule(LocalDataSourceModule())
            .remoteDataSourceModule(RemoteDataSourceModule())
            .build()
    }
}