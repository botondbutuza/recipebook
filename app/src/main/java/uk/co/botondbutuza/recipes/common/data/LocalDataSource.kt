package uk.co.botondbutuza.recipes.common.data

import io.reactivex.Maybe
import io.reactivex.Single
import io.realm.RealmObject
import uk.co.botondbutuza.recipes.common.data.models.Recipe


/**
 * Created by brotond on 26/01/2018.
 */

interface LocalDataSource {

    fun upsert(item: RealmObject)

    fun upsert(items: List<RealmObject>)

    fun clear(clazz: Class<out RealmObject>)


    fun recipes(): Maybe<List<Recipe>>

    fun recipe(name: String): Single<Recipe>
}
