package uk.co.botondbutuza.recipes.ui.main

import dagger.Component
import uk.co.botondbutuza.recipes.common.dagger.component.RepositoryComponent
import uk.co.botondbutuza.recipes.common.dagger.scope.ActivityScope

@ActivityScope
@Component(dependencies = [RepositoryComponent::class], modules = [MainModule::class])
interface MainComponent {

    fun inject(activity: MainActivity)
}