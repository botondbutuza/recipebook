package uk.co.botondbutuza.recipes.common.data.repository

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Maybe
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers

import uk.co.botondbutuza.recipes.common.data.LocalDataSource
import uk.co.botondbutuza.recipes.common.data.RemoteDataSource
import uk.co.botondbutuza.recipes.common.data.models.Recipe

class RecipeBookTest {

    private lateinit var remote: RemoteDataSource
    private lateinit var local: LocalDataSource
    private lateinit var dataRepository: RecipeBook

    private val recipe by lazy { Recipe(name = "pizza") }
    private val recipes by lazy { listOf(Recipe(name = "cookie"), recipe, Recipe(name = "paneer curry")) }

    @Before
    fun setUp() {
        remote = mock()
        local = mock()
        dataRepository = RecipeBook(local, remote)
    }


    @Test
    fun getFruits_remoteFirst() {
        whenever(local.recipes()).thenReturn(Maybe.never())
        whenever(remote.recipes()).thenReturn(Single.just(recipes))

        dataRepository
            .getRecipes()
            .test()
            .assertComplete()
            .assertValue(recipes)
    }

    @Test
    fun getFruits_localFirst() {
        whenever(local.recipes()).thenReturn(Maybe.just(recipes))
        whenever(remote.recipes()).thenReturn(Single.never())

        dataRepository
            .getRecipes()
            .test()
            .assertComplete()
            .assertValues(recipes)
    }

    @Test
    fun getFruit_error() {
        whenever(local.recipe(ArgumentMatchers.anyString())).thenReturn(Single.error(Exception("No element")))

        dataRepository
            .getRecipe(ArgumentMatchers.anyString())
            .test()
            .assertNotComplete()
            .assertErrorMessage("No element")
    }

    @Test
    fun getFruit() {
        whenever(local.recipe(ArgumentMatchers.anyString())).thenReturn(Single.just(recipe))

        dataRepository
            .getRecipe(ArgumentMatchers.anyString())
            .test()
            .assertComplete()
            .assertValue(recipe)
    }
}